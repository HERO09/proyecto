<?php if (!defined('CONTROLADOR')) exit; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title> Listar personajes </title>
        <link href="style.css">
    </head>
    <body>
        <style>
         body{
         background: lightblue;
          }

         h1{
         margin: 0;
    font-family: 'Times New Roman', Times, serif;
    
    background-repeat: no-repeat;
    background-size: 100% 1080px;
}
        </style>

        <center>
        <h1> Personajes </h1>
        <p> <a href="guardar_personaje.php"> Crear nuevo usuario </a> </p>
        <?php if (count($personajes) > 0): ?>
            <ul>
                <?php foreach ($personajes as $item): ?>
                 
                    <p> <?php echo $item['nombre'] . ' - ' . $item['descripcion']; ?>  </p>
                    <p> 
                        <a href="guardar_personaje.php?personaje_id=<?php echo $item['id'] ?>"> Editar </a> 
                        |
                        <a href="eliminar_personaje.php?personaje_id=<?php echo $item['id'] ?>"> Eliminar </a> 
                    </p>
                
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <p> No hay personajes para mostrar </p>
        <?php endif; ?>
</center>
    </body>
</html>