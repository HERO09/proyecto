<?php if (!defined('CONTROLADOR')) exit; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title> Guardar personaje </title>
        <link href="style.css">
    </head>
    <body>
    <style>
         body{
         background: lightblue;
          }

         h1{
         margin: 0;
    font-family: 'Times New Roman', Times, serif;
    
    background-repeat: no-repeat;
    background-size: 100% 1080px;
}
        </style>

        <center>
        <h1> Guardar personaje </h1>
        <form method="post" action="guardar_personaje.php">
            <label for="nombre"> Nombre </label>
            <br />
            <input type="text" name="nombre" id="nombre" value="<?php echo $personaje->getNombre() ?>" required />
            <br />
            <label for="descripcion"> Descripción </label>
            <br />
            <input type="text" name="descripcion" id="descripcion" value="<?php echo $personaje->getDescripcion() ?>" required />
            <br />            
            <?php if ($personaje->getId()): ?>
                <input type="hidden" name="personaje_id" value="<?php echo $personaje->getId() ?>" />
            <?php endif; ?>
            <input type="submit" value="Guardar" />
            <a href="index.php"> Cancelar </a>
        </form>
      </center>
    </body>
</html>